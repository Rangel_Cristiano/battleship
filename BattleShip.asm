.model small
.stack 100H
.data                    
      
matrizes DB "_____________   _____________   ____________________", 10, 13
         DB "|   TIROS   |   |   NAVIOS  |   |PLAYER 1          |", 10, 13
         DB "_____________   _____________   |      TIROS:      |", 10, 13
         DB "| 0123456789|   | 0123456789|   |    ACERTOS:      |", 10, 13
         DB "|0          |   |0          |   |__________________|", 10, 13
         DB "|1          |   |1          |   |PLAYER 2          |", 10, 13
         DB "|2          |   |2          |   |      TIROS:      |", 10, 13
         DB "|3          |   |3          |   |    ACERTOS:      |", 10, 13
         DB "|4          |   |4          |   |__________________|", 10, 13
         DB "|5          |   |5          |   |  AFUNDADOS:      |", 10, 13
         DB "|6          |   |6          |   |ULTIMO TIRO:      |", 10, 13
         DB "|7          |   |7          |   |__________________|", 10, 13
         DB "|8          |   |8          |                       ", 10, 13
         DB "|9          |   |9          |                       ", 10, 13
         DB "|___________|   |___________|                       ", 10, 13 
         DB "                                                    $"
         
mensagens DB "___________________________________________________ ", 10, 13
          DB "| Mensagem:       PLAYER                           |", 10, 13  
          DB "|                                                  |", 10, 13
          DB "|__________________________________________________|$"
       
tipos_barco    DB "_____________   ___________________________________________", 10, 13
               DB "|   NAVIOS  |   | EMBARCACAO      |  SIMBOLO  |  TAMANHO  |", 10, 13
               DB "_____________   |_________________|_______________________|", 10, 13
               DB "| 0123456789|   | PORTA AVIOES    |     A     |     5     |", 10, 13
               DB "|0          |   |_________________|_______________________|", 10, 13
               DB "|1          |   | NAVIO DE GUERRA |     B     |     4     |", 10, 13
               DB "|2          |   |_________________|_______________________|", 10, 13
               DB "|3          |   | SUBMARINO       |     S     |     3     |", 10, 13
               DB "|4          |   |_________________|_______________________|", 10, 13
               DB "|5          |   | DESTROYER       |     D     |     3     |", 10, 13
               DB "|6          |   |_________________|_______________________|", 10, 13
               DB "|7          |   | BARCO PATRULHA  |     P     |     2     |", 10, 13
               DB "|8          |   |_________________|_______________________|", 10, 13
               DB "|9          |                                              ", 10, 13
               DB "|___________|                                              ", 10, 13
               DB "                     ___________________________           ", 10, 13
               DB "                     |   Digite a posicao do   |           ", 10, 13
               DB "                     |                         |           ", 10, 13
               DB "                     |                         |           ", 10, 13
               DB "                     |                         |           ", 10, 13 
               DB "                     |_________________________|          $"

barcos1 DB 100 DUP (0)

msg_vitoria DB "Parabens ! Voce venceu a partida !!!   $" 
msg_sua_vez DB "Informe as coordernadas do Tiro: $"
limpa_edit_text DB "     $" 
msg_tiro_invalido DB "INVAL$"
 
barco_porta_avioes  DB "      PORTA AVIOES       $"
barco_de_guerra     DB "     NAVIO DE GUERRA     $"
barco_submarino     DB "        SUBMARINO        $"
barco_destroyer     DB "        DESTROYER        $"
barco_patrulha      DB "        PATRULHA         $"
barco_limpa         DB "                      $" 

counter_porta_avioes DB 5
counter_guerra DB 4
counter_submarino DB 3
counter_destroyer DB 3
counter_patrulha DB 2
counter_tiros_player1 DB 0
counter_tiros_player2 DB 0
counter_acertos_player1 DB 0
counter_acertos_player2 DB 0
counter_afundados DB 0
player DB 1

ASCII_PORTA_AVIOES EQU 65 
ASCII_GUERRA EQU 66
ASCII_SUBMARINO EQU  83
ASCII_DESTROYER EQU  68
ASCII_PATRULHA EQU 80
COM1 EQU 0
COM2 EQU 1
NUM_CHAR_LINHA EQU 54 
ZERO EQU '0'
NOVE EQU '9'
QUADRADO EQU 254  

.code
       
escreve_escolhe_barco proc
    xor dx, dx
    call set_cursor   
                                  
    mov dx, offset tipos_barco
    call esc_string 
             
    call leitura_posicionamento_navios                     
       
    ret
endp


set_cursor proc; dh->linha  dl->coluna 
    push bx
    push ax
    
    mov bh, 0  ;pagina
    mov ah, 2  ;seta cursor
    int 10h
    
    pop ax
    pop bx  
    
    ret
endp
    
le_char PROC
    mov ah, 07h
    int 21h  
    xor ah, ah
    ret
ENDP

esc_char PROC   ;escreve caractere em DL
    push ax
    mov ah,02H
    int 21h
    pop ax
    ret
ENDP 

esc_string proc ; escreve string em dx, terminada em $  
    push ax
    mov ah, 09H
    int 21H
    pop ax
    ret
endp

leitura_posicionamento_navios PROC     
    push ax
    push bx 
    push cx
    push dx  
                
    xor ax, ax
    xor cx, cx
    mov ch, 05h
    ;Seleciona tipo do barco para escrever no retangulo 
  PROXIMO_BARCO:
    dec ch                
    call prepara_retangulo_barco             
                   
    mov cl, 03h
             
  LEITURA:
        call le_char
        cmp cl, 01h
        jz DIRECAO
                
        cmp al, ZERO  
        jb LEITURA
        cmp al, NOVE
        ja LEITURA
                                    
        jmp VALIDO                   
  DIRECAO:
        cmp al, 'H'
        jz VALIDO 
        cmp al, 'h'
        jz VALIDO 
        cmp al, 'V'
        jz VALIDO  
        cmp al, 'v'
        jz VALIDO
        jmp LEITURA                
  VALIDO:   
        mov dl, al
        call esc_char   ;escreve o caractere que leu na tela
        sub ax, ZERO
        push ax
        dec cl
        or cl, cl
        jnz REP_INVALIDO
        jmp VALIDAR_POSICAO
  REP_INVALIDO:
        jmp LEITURA
  VALIDAR_POSICAO:   ;Descobre se o posicionamento eh vertical ou horizontal
        pop ax      ;Ignorar a orientacao do barco na pilha
            
        pop ax
        mov ah, al
        xor al, al
        pop bx
        add ax, bx  ;ah->col e al->lin
        call fazer_validacao 

        or ch, ch
        jnz PROXIMO_BARCO
         
        pop dx
        pop cx 
        pop bx
        pop ax
        ret
endp  

prepara_retangulo_barco PROC    ;Prepara o retangulo com o nome do barco. CH->Codigo do barco a ser escrito
    push dx

    cmp ch, 04H
    jz PORTA_AVIOES
    cmp ch, 03H
    jz GUERRA
    cmp ch, 02H
    jz SUBMARINO
    cmp ch, 01H
    jz DESTROYER
    jmp PATRULHA
    
  PORTA_AVIOES: 
    mov dx, offset barco_porta_avioes
    jmp ESCREVE_TIPO_BARCO
  GUERRA: 
    mov dx, offset barco_de_guerra
    jmp ESCREVE_TIPO_BARCO
  SUBMARINO: 
    mov dx, offset barco_submarino
    jmp ESCREVE_TIPO_BARCO
  DESTROYER: 
    mov dx, offset barco_destroyer
    jmp ESCREVE_TIPO_BARCO
  PATRULHA: 
    mov dx, offset barco_patrulha
    jmp ESCREVE_TIPO_BARCO 
    
  ESCREVE_TIPO_BARCO:
    ;Coloca o cursor no inicio do retangulo
    push dx         
    mov dh, 17 ;linha
    mov dl, 22 ;coluna
    call set_cursor  
     
    ;Escreve o nome do barco
    pop dx 
    call esc_string
    
    ;Apaga a linha que contem o posicioonamento do ultimo barco 
    mov dh, 19 ;linha
    mov dl, 22 ;coluna
    call set_cursor  
    
    mov dx, offset barco_limpa 
    call esc_string
    
    ;Coloca o cursor no local correto para o usuario   
    mov dh, 19 ;linha
    mov dl, 33 ;coluna
    call set_cursor


    pop dx
    ret
endp

fazer_validacao proc ;Valida a poicao dos barcos e escreve os mesmos na tela. AH->coluna AL->linha CH->codigo do barco DL->character de orientacao
        push ax
        push bx
        push dx

        xor bx, bx         
                 
        cmp dl, 'V'
        jz EH_VERTICAL 
        cmp dl, 'v'
        jz EH_VERTICAL


        mov bl, ah  ;Posicionamento horizontal
        jmp VALIDA
  EH_VERTICAL:
        mov bl, al
           
  VALIDA:             ;Descobre se o barco cabe na tela
        mov bh, bl
        dec bh
        add bl, ch
        cmp ch, 02H ;Pegamos a partir do codigo dele(ex:codigo 05 tem tamanho 5). Porem, no destroyer e no patrulha, precissamos somar um 
        jb SOMA_UM
        jmp FAZ_NADA
        
  SOMA_UM: 
        inc bl
  FAZ_NADA:
        cmp bl, 0AH     ;BX tem a ultima posicao do barco, e 0AH se refere a borda da tela
        jb  VER_SOBREPOSICAO  
        inc ch
        jmp FINALIZAR_VALIDACAO
        
  VER_SOBREPOSICAO:     ;Descobre se o barco nao ficara sobreposto 
        add bl, 02H     ;Ultima posicao do barco, mais um. E mais um novamente em razao  do inicio do vetor
                 
        cmp dl, 'H'
        jz EH_HORIZONTAL 
        cmp dl, 'h'
        jz EH_HORIZONTAL 
                  
        ;EH_VERTICAL
        mov dx, ax
        mov al, 0AH
        mul dl
        inc dh 
        add al,dh   ;index no array/matriz
        mov dh, dl 
        inc dl
        
  REP_VERTICAL: 
        cmp dl, bl
        jz ATUALIZA_MATRIZ_V
        push bx
        xor bx, bx
        mov bl, al
        sub bx, 0AH
        test barcos1[bx], 0FFH 
        pop bx
        jz TA_LIVRE_V
         
        inc ch
        jmp FINALIZAR_VALIDACAO
  TA_LIVRE_V:
        add al, 0AH  ;Posicao no vetor
        inc dl       ;Linha
        jmp REP_VERTICAL 
      
  ATUALIZA_MATRIZ_V:
        dec dl 
        sub al, 0AH
  REP_ATUALIZA_MATRIZ_V:
        call PREPARA_MATRIZ
    
        sub ax, 0AH 
        dec dl
        cmp dh, dl
        jnz REP_ATUALIZA_MATRIZ_V
        jmp FINALIZAR_VALIDACAO
 
  EH_HORIZONTAL:
        mov dx, ax
        mov al, 0AH
        mul dl
        mov dl, dh  ;Posicao inicial, menos um
        inc dh      
        add al,dh   ;index no array/matriz 
                      
  REP_HORIZONTAL:
        cmp dh, bl
        jz ATUALIZA_MATRIZ_H
        push bx
        xor bx, bx
        mov bl, al
        dec bx
        test barcos1[bx], 0FFH 
        pop bx
        jz TA_LIVRE_H
         
        inc ch
        jmp FINALIZAR_VALIDACAO
  TA_LIVRE_H:
        inc al  ;Posicao no vetor
        inc dh  ;Coluna
        jmp REP_HORIZONTAL 
      
  ATUALIZA_MATRIZ_H:
        dec dh 
        dec ax
  REP_ATUALIZA_MATRIZ_H:
        call prepara_matriz 
        
        dec ax
        dec dh
        cmp dh, dl
        jnz REP_ATUALIZA_MATRIZ_H

  FINALIZAR_VALIDACAO:
    pop dx
    pop bx
    pop ax
    ret
endp

prepara_matriz PROC  ;Recebe em AX a posicao no vetor e em DX as posicoes maximas e minimas da tabela
    push bx
    push dx    
    mov bx, ax         
        
    call char_do_barco
    
    push dx 
    mov barcos1[bx], dl
    mov dl, 0AH    
    dec ax
    div dl
    mov dl, ah
    add dl, 02H
    mov dh, al
    add dh, 04H
    
    call set_cursor  
    pop dx
    call esc_char          
    pop dx
    
    mov ax, bx
    pop bx
    
    ret
endp

char_do_barco PROC ;Recebe em CH o codigo do barco e devolve em AX, seu char correspondente       
    cmp ch, 04H
    jz PORTA_AVIOES_ASCII
    cmp ch, 03H
    jz GUERRA_ASCII
    cmp ch, 02H
    jz SUBMARINO_ASCII
    cmp ch, 01H
    jz DESTROYER_ASCII
    jmp PATRULHA_ASCII      
        
  PORTA_AVIOES_ASCII: 
    mov dx, ASCII_PORTA_AVIOES
    ret
  GUERRA_ASCII: 
    mov dx, ASCII_GUERRA
    ret 
  SUBMARINO_ASCII: 
    mov dx, ASCII_SUBMARINO                          
    ret
  DESTROYER_ASCII: 
    mov dx, ASCII_DESTROYER
    ret
  PATRULHA_ASCII: 
    mov dx, ASCII_PATRULHA
    ret
endp
     
limpa_tela PROC ; Limpa a tela
    push ax
    mov ah, 00h 
    mov al, 03h 
    int 10h 
    pop ax 
    ret 
endp

escreve_uint16 PROC   ; Escreve um numero armazenado em AX
        push ax
        push bx
        push cx
        push dx
        
        xor cx, cx
        mov bx,10
  DIVIDE: 
        xor dx,dx    
        div bx      ; obtem a unidade do numero
        add dl,'0'  ; transforma em caractere
        push dx     ; empilha caractere
        inc cx
        or ax, ax
        jnz DIVIDE


  ESCREVE: 
        pop dx      ; Laco que escreve os caracteres empilhados
        call ESC_CHAR
        loop ESCREVE
        
        pop dx
        pop cx
        pop bx
        pop ax
        ret
endp

leitura_coordenadas PROC
    push bx
    push cx
    push dx
    
        mov cx,2
        xor bx,bx
        xor ax,ax 
        xor dx,dx
  LE_COORDENADAS:
        push ax
        cmp cx, 0   ; Ate ler dois numeros
        jz FIM_LE_COORDENADAS
        call LE_CHAR        
        cmp al, ZERO ; Verifica se eh valido 
        jb ERROCHAR
        cmp al, NOVE
        ja ERROCHAR ; Verifica se eh valido
        mov dl, al
        call ESC_CHAR   ; Escreve o caractere na tela
        
        xor ah, ah   
        sub al, ZERO  ;transforma numero
        mov bx,ax
        pop ax
        mov dx, 10        
        mul dx
        add ax,bx
        dec cx
        jmp LE_COORDENADAS
        
  ERROCHAR: ;se nao digitar caracteres validos
        pop ax
        jmp LE_COORDENADAS
    
  FIM_LE_COORDENADAS:
        pop ax
        pop dx
        pop cx
        pop bx
        ret
ENDP        


escreve_texto_colorido proc
    mov ax, 0920h     
    int 10h         
    int 21H         
    ret
endp

le_char_video proc ;retorna em al o char ascii e ah os atributos da cor
    push bx
    mov bh, 0
    mov ah, 08
    int 10h
    pop bx
    ret
endp

escreve_primeira_tabela proc
    push ax
    
    pop ax
    ret
endp

verifica_tiro proc
    push ax
    mov bx, 10
    div bl
    
    ;pinta na SEGUNDA tabela 
    mov dh, 04h
    add dh, al
    mov dl, 18
    add dl, ah 
    call set_cursor 
    
    call le_char_video
    
    cmp ah, 0004h ;verifica se ja esta vermelho(se ja foi disparado ali)
    jnz TIRO_VALIDO
    mov dh, 0Ah
    mov dl, 46
    call set_cursor
    mov dx, offset msg_tiro_invalido
    call esc_string  ;avisa na tabela que tiro nao valeu
    jmp TIRO_INVALIDO

  TIRO_VALIDO:
    cmp al, ' '  ;compara com espaco vazio
    jnz ESC_VERMELHO ;so pinta a letra de vermelho
    mov al, QUADRADO ;escreve quadrado
  ESC_VERMELHO: 
    mov bx, 0004h ;Seleciona a cor vermelha e a pagina 0  
    mov cx, 1h ; Numero de caracteres que serao printados   
    call escrever_chars_coloridos 
    
    ;pinta na PRIMEIRA tabela   
    sub dl, 16
    call set_cursor
    
    cmp al, QUADRADO
    jz MARCAR_TIRO_ERRADO
    mov dl, 'O'
    call esc_char
    jmp ATUALIZA_NA_TABELA
  MARCAR_TIRO_ERRADO:
    mov al, 'X'
    call escrever_chars_coloridos 
    
  ATUALIZA_NA_TABELA:
    call atualiza_tabela
  
  TIRO_INVALIDO:
    pop ax
    ret
endp 

atualiza_tabela proc
    push ax
    xor ah,ah
    mov bl, al
    
    ;acrescenta os tiros disparados na tabela   
    cmp player, 1
    jnz CONTA_TIRO_PLAYER2
    
    mov dh, 02h
    mov dl, 46
    call set_cursor
    add counter_tiros_player1, 1    
    xor ax, ax
    mov al, counter_tiros_player1
    call escreve_uint16
    jmp VERIFICA_ACERTO

  CONTA_TIRO_PLAYER2:   
    mov dh, 06h
    mov dl, 46
    call set_cursor 
    add counter_tiros_player2, 1
    xor ax, ax
    mov al, counter_tiros_player2
    call escreve_uint16

  VERIFICA_ACERTO:
    ;verifica se acertou barco e se afundou algum
    cmp bl, 'A'
    jz ACERTOU_AVIOES
    cmp bl, 'B'
    jz ACERTOU_GUERRA
    cmp bl, 'S'
    jz ACERTOU_SUBMARINO
    cmp bl, 'D'
    jz ACERTOU_DESTROYER
    cmp bl, 'P'
    jz ACERTOU_PATRULHA
    
    ;errou tiro, troca player
    mov dh, 12H
    mov dl, 19H
    call set_cursor 
    xor ax, ax
    
    cmp player, 1
    jz TROCOU_PLAYER
    mov player, 1   
    mov al, player
    call escreve_uint16
    jmp CONTOU
    
  TROCOU_PLAYER:
    mov player, 2
    mov al, player
    call escreve_uint16
    jmp CONTOU
    
ACERTOU_AVIOES: 
    sub counter_porta_avioes, 1
    jz AFUNDOU
    jmp MARCA_PONTO
ACERTOU_GUERRA: 
    sub counter_guerra, 1
    jz AFUNDOU
    jmp MARCA_PONTO
ACERTOU_SUBMARINO:  
    sub counter_submarino, 1
    jz AFUNDOU
    jmp MARCA_PONTO
ACERTOU_DESTROYER:  
    sub counter_destroyer, 1
    jz AFUNDOU
    jmp MARCA_PONTO
ACERTOU_PATRULHA:   
    sub counter_patrulha, 1
    jz AFUNDOU
    jmp MARCA_PONTO
    
  AFUNDOU:
    mov dh, 9h
    mov dl, 46
    call set_cursor 
    add counter_afundados, 1
    xor ax, ax
    mov al, counter_afundados
    call escreve_uint16

  MARCA_PONTO: ;marca que acertou um barco
    cmp player, 1
    jnz MARCA_PLAYER2
    
    mov dh, 03h
    mov dl, 46
    call set_cursor 
    add counter_acertos_player1, 1
    xor ax, ax
    mov al, counter_acertos_player1
    call escreve_uint16
    jmp CONTOU
    
  MARCA_PLAYER2:    
    mov dh, 07h
    mov dl, 46
    call set_cursor 
    add counter_acertos_player2, 1
    xor ax, ax
    mov al, counter_acertos_player2
    call escreve_uint16

  CONTOU:
    pop ax
    ret
endp    

pintar_quadrados proc    ;Pinta todos os quadrados de verde         
    push ax
    push bx
    push cx 
    push dx
    
    xor cx, cx
    
  COLORIR_H:    
    mov dh, 04h
    add dh, ch
    mov dl, 02h
    add dl, cl 
    call set_cursor
            
    mov dx, cx
    mov AL, QUADRADO
    mov BL, 02H         ;Verde
    mov cx, 0AH         ;Pinta a linha inteira
    call escrever_chars_coloridos
    mov cx, dx            
          
    inc ch
    cmp ch, 0AH
    jnz COLORIR_H       
          
    pop dx
    pop cx      
    pop bx
    pop ax
    
    ret        
endp

escrever_chars_coloridos proc ;Escreve colorido na tela, CX->quantos caracteres, AL->character a escrever, BL->Cor
    push cx 
    push ax
    
    xor bh, bh
    mov ah, 09H
    
    int 10H
          
    pop ax
    pop cx
    
    ret
endp  

preenche_barcos proc    ;Preenche visualmente a matriz de barcos escolhida peelo usuario anteriormente
    push ax 
    push bx
    push cx
    push dx
         
    xor ax, ax    
    xor bx, bx
    xor cx, cx
  
  PREENCHE_H:    
    inc bx
    test barcos1[bx], 0FFH
    jz INC_PREENCHE  ;Se tiver algo, escreve a sigla que esta nessa posicao
    inc cx 
    
    mov dh, 04h
    add dh, ah 
    mov dl, 12h
    add dl, al 
    call set_cursor
    
    mov dl, barcos1[bx]
    call esc_char
    
    cmp cx, 11H  ;Permanece no laco ate encontrar as 17 posicoes ocupadas
    jz FIM_PREENCHE

  INC_PREENCHE:
    inc al
    cmp al, 0AH
    jnz PREENCHE_H
    xor al, al
    inc ah
    cmp ah, 0AH
    jnz PREENCHE_H
  FIM_PREENCHE:
    pop dx
    pop cx
    pop bx
    pop ax     
    
    ret
endp


inicializa_matrizes proc
   push dx
   mov dx, offset matrizes
   call esc_string

   pop dx 
   ret
endp    

seu_turno proc   
    mov dh, 13H
    mov dl, 23H
    call set_cursor 
    call leitura_coordenadas
    
    ;escreve na tabela a ultima coordenada digitada 
    mov dh, 0Ah
    mov dl, 46
    call set_cursor
    mov dx, offset limpa_edit_text 
    call esc_string ;antes limpa
    
    mov dh, 0Ah
    mov dl, 46
    call set_cursor
    call escreve_uint16
    
    mov ah, 01 
    mov dx, COM1;  escreve na porta COM1             
    int 14h
  
    mov ah, 01 
    mov dx, COM2; escreve na porta COM2              
    int 14h   

    ret
endp 

leitura_serial_port proc        
  lacop1: mov dx, COM1  ;leitura COM1 
    mov ah, 2h       
    int 14h
    test ah, 80h
    jnz lacop1  
    
    call verifica_tiro
    
   ret
endp 
      
inicio: mov ax,@data
        mov ds, ax
         
        ;text mode. 80x25. 16 colors. 8 pages.
        mov al, 03h  
        mov ah, 0     
        int 10h
        
        call limpa_tela
        
        call escreve_escolhe_barco 
        
        call limpa_tela            
        
        call inicializa_matrizes
       
        call pintar_quadrados
        
        call preenche_barcos
        
        xor dx, dx  ;carrega interface das mensagens
        mov dh, 11H
        call set_cursor                                     
        
        mov dx, offset mensagens
        call esc_string 

        mov dx , COM1
        mov ah, 0  ;inicializa porta COM1     
        int 14h

        mov dx , COM2
        mov ah, 00  ;inicializa porta COM2    
        int 14h     
        
        ;seta o player que comecara o jogo
        mov dh, 12H
        mov dl, 19H
        call set_cursor 
        xor ax, ax
        mov al, player
        call escreve_uint16
        
        ;avisa para digitar as coordenadas
        mov dh, 13H
        mov dl, 2H
        call set_cursor     
        mov dx, offset msg_sua_vez
        call esc_string 
            
  PROX_RODADA:  
        call seu_turno ;escreve msg avisando e seta cursor  
        call leitura_serial_port ;le dado da porta e confere se acretou algum barco
        
        mov dh, 13H
        mov dl, 23H
        call set_cursor   
        mov dx, offset limpa_edit_text 
        call esc_string
            
        cmp counter_afundados, 5  ; se afundou todos os navios acaba o jogo
        jz FIM_DO_JOGO
        
        jmp PROX_RODADA
        
  FIM_DO_JOGO:
        mov dh, 12H
        mov dl, 19H
        call set_cursor 
        
        ;verifica quem ganhou
        mov al, counter_acertos_player1
        cmp al, counter_acertos_player2
        ja PLAYER1_VENCEU
        mov al, 2
        call escreve_uint16
        jmp PARABENIZAR_VITORIA
  PLAYER1_VENCEU:
        mov al, 1
        call escreve_uint16
  
  PARABENIZAR_VITORIA:
        mov dh, 13H
        mov dl, 2H
        call set_cursor   
     
        mov dx, offset msg_vitoria
        call esc_string 
        
        mov dh, 16H
        mov dl, 0H
        call set_cursor ;escreve o endereco do arquivo fora da interface do jogo
  
        return: mov ah, 04CH
        int 21H
    ret
end inicio
